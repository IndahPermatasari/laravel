@extends('layouts.app')

@section('content')
<div class="container" style="width: 70%; margin: 0 auto">
    <div class="row">
        <div class="col-8">
            <img src="/storage/{{ $post->image }}" class="w-100">
        </div>
        <div class="col-4">
            <div>
                <div class="d-flex align-items-center">
                    <div class="pr-2">
                        <img src="{{ $post->user->profile->profileImage() }}" style="border: 1px solid black; max-width: 40px" class="w-100 rounded-circle">
                    </div>
                    <div class="font-weight-bold">
                        <a href="/profile/{{ $post->user->id }}" style="text-decoration:none">
                            <span class="text-dark">{{ $post->user->username }}</span>
                        </a>
                        |
                        <a href="#">Follow</a>
                    </div>
                </div>
                <hr>
                <p>
                    <span class="font-weight-bold">
                        <a href="/profile/{{ $post->user->id }}" style="text-decoration:none">
                            <span class="text-dark">{{ $post->user->username }} </span>
                        </a>
                    </span> {{ $post->caption }}
                </p>
            </div>
        </div>
    </div>
</div>
@endsection